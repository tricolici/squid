docker run --name squid -d \
  -v $(pwd)/cache:/var/cache/squid \
  -v $(pwd)/logs:/var/log/squid \
  -p 3128:3128 squid

docker logs -f squid
