#!/bin/bash
set -e

if [[ ! -d /var/cache/squid/00 ]]; then
  echo "Initializing cache..."
  /usr/sbin/squid -N -f /etc/squid/squid.conf -z
fi

exec /usr/sbin/squid -f /etc/squid/squid.conf -NYCd 10
