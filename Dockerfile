FROM alpine:3.7
RUN apk --no-cache add bash squid
ADD files /
EXPOSE 3128/tcp
VOLUME [ "/var/cache/squid" ]
VOLUME [ "/var/log/squid" ]
ENTRYPOINT ["/entrypoint.sh"]
